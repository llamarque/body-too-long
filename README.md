The function `doSendFile` require the parameters `offset` / `length`
> https://github.com/eclipse-vertx/vert.x/blob/3.6/src/main/java/io/vertx/core/http/impl/HttpServerResponseImpl.java#L426
>
> ```java
> private void doSendFile(String filename, long offset, long length, Handler<AsyncResult<Void>> resultHandler) {
>     ...
> }
> ```

When called by `sendFile` :
- w/o handler, the parameters are correct
    > https://github.com/eclipse-vertx/vert.x/blob/3.6/src/main/java/io/vertx/core/http/impl/HttpServerResponseImpl.java#L370-L374
    >
    > ```java
    > @Override
    > public HttpServerResponseImpl sendFile(String filename, long offset, long length) {
    >     doSendFile(filename, offset, length, null);
    >     return this;
    > }
    > ```
- w/ handler, the second parameter is wrong, end offset instead of length
    > https://github.com/eclipse-vertx/vert.x/blob/3.6/src/main/java/io/vertx/core/http/impl/HttpServerResponseImpl.java#L376-L380
    > ```java
    > @Override
    > public HttpServerResponse sendFile(String filename, long start, long end, Handler<AsyncResult<Void>> resultHandler) {
    >     doSendFile(filename, start, end, resultHandler);
    >     return this;
    > }
    > ```

In our case, the `StaticHandler` use the function w/ handler
> https://github.com/vert-x3/vertx-web/blob/master/vertx-web/src/main/java/io/vertx/ext/web/handler/impl/StaticHandlerImpl.java#L403-L407
>
> ```java
> return request.response().sendFile(file, finalOffset, finalEnd + 1, res2 -> {
>     if (res2.failed()) {
>         context.fail(res2.cause());
>     }
> });
> ```

When the reproducer, the header content-length and the body size are correct
> `12:23:52.387 [vert.x-eventloop-thread-0] INFO  fr.edu.acdijon.client.MainVerticle - Content-Length = 1024`
> `12:23:52.387 [vert.x-eventloop-thread-0] INFO  fr.edu.acdijon.client.MainVerticle - Body length = 1024`

but more data than required were returned
> `12:23:52.373 [vert.x-eventloop-thread-0] INFO  io.vertx.ext.web.handler.impl.LoggerHandlerImpl - 172.30.102.108 - - [Mon, 10 Dec 2018 11:23:52 GMT] "GET /somedir/range.bin HTTP/1.1" 206 2048 "-" "Vert.x-WebClient/3.5.4"`

the client try to interpret the remaining bytes as another http response.
> `12:23:52.386 [vert.x-eventloop-thread-0] ERROR io.vertx.core.net.impl.ConnectionBase - invalid version format: D P¬ÈQWÖ~ÞÒP3IIÆ+Üº ¤<ÂYUGÇB¾L¦P%Ñ7^({Ü~]O«ÊÇÐ`

When downloading a large file (e.g. 3Gb) by chunk of 5Mb, we can see the latency increase until the mid of the file before decreasing until the end.

![latency](/uploads/a426d405834137a0453ad33f0974715f/latency.png)
