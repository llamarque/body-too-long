package fr.edu.acdijon.client;

import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Log4J2LoggerFactory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.handler.LoggerHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class MainVerticle extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(MainVerticle.class);

    private static final String HOST = "0.0.0.0";
    private static final int PORT = 8080;

    private static final String RANGE = "bytes=%d-%d";

    private static final boolean LOG_SERVER = false;
    private static final boolean LOG_CLIENT = true;

    private WebClient client;

    @Override
    public void start() throws Exception {
        InternalLoggerFactory.setDefaultFactory(Log4J2LoggerFactory.INSTANCE);

        client = WebClient.create(
            vertx,
            new WebClientOptions().setLogActivity(LOG_CLIENT));

        Router router = Router.router(vertx);
        router.route()
            .handler(LoggerHandler.create())
            .handler(StaticHandler.create().setEnableRangeSupport(true));

        vertx.createHttpServer(new HttpServerOptions().setLogActivity(LOG_SERVER))
            .requestHandler(router::accept)
            .listen(PORT, HOST, listenHandler -> {
                client.get(PORT, HOST, "/somedir/range.bin")
                                                                               //                       min(filesize - start, end + 1)
                    // .putHeader("Range", String.format(RANGE, 0, 1023))      // resp size = 1024 ===> min(5120 - 0,    1023 + 1)
                    .putHeader("Range", String.format(RANGE, 1024, 2047))      // resp size = 2048 ===> min(5120 - 1024, 2047 + 1)
                    // .putHeader("Range", String.format(RANGE, 2048, 3071))   // resp size = 3072 ===> min(5120 - 2048, 3071 + 1)
                    // .putHeader("Range", String.format(RANGE, 3072, 4095))   // resp size = 2048 ===> min(5120 - 3072, 4095 + 1)
                    // .putHeader("Range", String.format(RANGE, 4096, 5119))   // resp size = 1024 ===> min(5120 - 4096, 5119 + 1)
                    .send(this::getRange);
            });
    }

    private void getRange(AsyncResult<HttpResponse<Buffer>> response) {
        logger.info("Content-Length = {}", response.result().getHeader("Content-Length"));
        logger.info("Body length = {}", response.result().body().length());
    }
}
